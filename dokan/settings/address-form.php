<?php
/**
 * Dokan Settings Address form Template
 *
 * @since 2.4
 *
 * @package dokan
 */

$address         = isset( $profile_info['address'] ) ? $profile_info['address'] : '';
$address_street1 = isset( $profile_info['address']['street_1'] ) ? $profile_info['address']['street_1'] : '';
$address_street2 = isset( $profile_info['address']['street_2'] ) ? $profile_info['address']['street_2'] : '';
$address_city    = isset( $profile_info['address']['city'] ) ? $profile_info['address']['city'] : '';
$address_zip     = isset( $profile_info['address']['zip'] ) ? $profile_info['address']['zip'] : '';
$address_country = isset( $profile_info['address']['country'] ) ? $profile_info['address']['country'] : '';
$address_state   = isset( $profile_info['address']['state'] ) ? $profile_info['address']['state'] : '';
$address_district   = isset( $profile_info['address']['district'] ) ? $profile_info['address']['district'] : '';
?>

<input type="hidden" id="dokan_selected_country" value="<?php echo esc_attr( $address_country )?>" />
<input type="hidden" id="dokan_selected_state" value="<?php echo esc_attr( $address_state ); ?>" />
<div class="dokan-form-group">
    <label class="dokan-w3 dokan-control-label" for="setting_address"><?php esc_html_e( 'Address', 'dokan-lite' ); ?></label>

    <div class="dokan-w5 dokan-text-left dokan-address-fields">
        <?php if ( $seller_address_fields['street_1'] ) { ?>
            <div class="dokan-form-group">
                <label class="dokan-w3 control-label" for="dokan_address[street_1]"><?php esc_html_e( 'Street ', 'dokan-lite' ); ?>
                    <?php
                    $required_attr = '';
                    if ( $seller_address_fields['street_1']['required'] ) {
                        $required_attr = 'required'; ?>
                        <span class="required"> *</span>
                    <?php } ?>
                </label>
                <input <?php echo esc_attr( $required_attr ); ?> <?php echo esc_attr( $disabled ) ?> id="dokan_address[street_1]" value="<?php echo esc_attr( $address_street1 ); ?>" name="dokan_address[street_1]" placeholder="<?php esc_attr_e( 'Street address' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
            </div>
        <?php }
        if ( $seller_address_fields['street_2'] ) { ?>
            <div class="dokan-form-group">
                <label class="dokan-w3 control-label" for="dokan_address[street_2]"><?php esc_html_e( 'Street 2', 'dokan-lite' ); ?>
                    <?php
                    $required_attr = '';
                    if ( $seller_address_fields['street_2']['required'] ) {
                        $required_attr = 'required'; ?>
                        <span class="required"> *</span>
                    <?php } ?>
                </label>
                <input <?php echo esc_attr( $required_attr ); ?> <?php echo esc_attr( $disabled ) ?> id="dokan_address[street_2]" value="<?php echo esc_attr( $address_street2 ); ?>" name="dokan_address[street_2]" placeholder="<?php esc_attr_e( 'Apartment, suite, unit etc. (optional)' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
            </div>
        <?php }
        if ( $seller_address_fields['city'] || $seller_address_fields['zip'] ) {
        ?>
            <div class="dokan-from-group">
                <?php if ( $seller_address_fields['city'] ) { ?>
                    <div class="dokan-form-group dokan-w6 dokan-left dokan-right-margin-30">
                        <label class="control-label" for="dokan_address[city]"><?php esc_html_e( 'City', 'dokan-lite' ); ?>
                            <?php
                            $required_attr = '';
                            if ( $seller_address_fields['city']['required'] ) {
                                $required_attr = 'required'; ?>
                                <span class="required"> *</span>
                            <?php } ?>
                        </label>
                        <input <?php echo esc_attr( $required_attr ); ?> <?php echo esc_attr( $disabled ) ?> id="dokan_address[city]" value="<?php echo esc_attr( $address_city ); ?>" name="dokan_address[city]" placeholder="<?php esc_attr_e( 'Town / City' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
                    </div>
                <?php }
                if ( $seller_address_fields['zip'] ) { ?>
                    <div class="dokan-form-group dokan-w5 dokan-left">
                        <label class="control-label" for="dokan_address[zip]"><?php esc_html_e( 'Post/ZIP Code', 'dokan-lite' ); ?>
                            <?php
                            $required_attr = '';
                            if ( $seller_address_fields['zip']['required'] ) {
                                $required_attr = 'required'; ?>
                                <span class="required"> *</span>
                            <?php } ?>
                        </label>
                        <input <?php echo esc_attr( $required_attr ); ?> <?php echo esc_attr( $disabled ) ?> id="dokan_address[zip]" value="<?php echo esc_attr( $address_zip ); ?>" name="dokan_address[zip]" placeholder="<?php esc_attr_e( 'Postcode / Zip' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
                    </div>
                <?php } ?>
                <div class="dokan-clearfix"></div>
            </div>
        <?php }

        if ( $seller_address_fields['country'] ) {
            $country_obj   = new WC_Countries();
            $countries     = $country_obj->countries;
            $states        = $country_obj->states;
        ?>
            <div class="dokan-form-group">
                <label class="control-label" for="dokan_address[country]"><?php esc_html_e( 'Country ', 'dokan-lite' ); ?>
                    <?php
                    $required_attr = '';
                    if ( $seller_address_fields['country']['required'] ) {
                        $required_attr = 'required'; ?>
                        <span class="required"> *</span>
                    <?php } ?>
                </label>
                <select <?php echo esc_attr( $required_attr ); ?> disabled name="dokan_address[country]" class="country_to_state dokan-form-control" id="dokan_address_country">
                    <option>Ukrajna</option>
                </select>
            </div>
        <?php } ?>
            <div class="dokan-form-group">
                <label class="dokan-w3 control-label" for="dokan_address[state]"><?php esc_html_e( 'State ', 'dokan-lite' ); ?></label>
                <select <?php echo esc_attr( $required_attr ); ?> disabled name="dokan_address[state]" class="dokan-form-control" id="dokan_address_state">
                    <option>Kárpátalja</option>
                </select>

            </div>
            <!-- Jarasok -->
            <div  id="dokan-district-box" class="dokan-form-group">
                <label class="dokan-w3 control-label" for="dokan_address[district]"><strong>Járás</strong>
                        <span class="required"> *</span>
                </label>
                <select <?php echo esc_attr( $required_attr ); ?> <?php echo esc_attr( $disabled ) ?> name="dokan_address[district]" class="dokan-form-control" id="dokan_address_district">
                    <?php
                    $selected_district = esc_attr( $address_district );
                    $options = jarasok( 'get' );
                    $options = array_merge( ['' => ''], $options );
                    foreach ( $options as $key => $option ) {
                        echo '<option value="' . esc_attr( $key ) . '"'
                             . selected( $selected_district, $key, false ) . '>'
                             . esc_html__( $option, 'wpselected' ) . '</option>';
                    }
                    ?>
                </select>

            </div>
    </div>
</div>
