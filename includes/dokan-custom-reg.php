<?php
/**
* Custom registration form shortcode
* @author Sabolch Greba <greba.szabolcs@gmail.com>
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_shortcode( 'dokan_custom_reg', 'dokan_custom_reg_form' );

function dokan_custom_reg_form() {
   if ( is_admin() ) return;
   //if ( is_user_logged_in() ) return;

    $dokan_assets = new WeDevs\Dokan\Assets();
    $dokan_assets->load_form_validate_script();

    wp_enqueue_script( 'dokan-vendor-registration' );
    ob_start();
?>
<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<div class="row" id="customer_login">

    <div class="col-md-12 reg-form">

        <!-- <h2><?php esc_html_e( 'Register', 'dokan-theme' ); ?></h2> -->

        <form id="register" method="post" class="register">
            <?php do_action( 'woocommerce_register_form_start' ); ?>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                <p class="form-row form-group form-row-wide">
                    <label for="reg_username"><?php esc_html_e( 'Username', 'dokan-theme' ); ?> <span class="required">*</span></label>
                    <input type="text" class="input-text form-control" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) esc_attr( $_POST['username'] ); ?>" required="required" />
                </p>

            <?php endif; ?>

            <p class="form-row form-group form-row-wide">
                <label for="reg_email"><?php esc_html_e( 'Email address', 'dokan-theme' ); ?> <span class="required">*</span></label>
                <input type="email" class="input-text form-control" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) esc_attr($_POST['email']); ?>" required="required" />
            </p>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                <p class="form-row form-group form-row-wide">
                    <label for="reg_password"><?php esc_html_e( 'Password', 'dokan-theme' ); ?> <span class="required">*</span></label>
                    <input type="password" class="input-text form-control" name="password" id="reg_password" value="<?php if ( ! empty( $_POST['password'] ) ) esc_attr( $_POST['password'] ); ?>" required="required" minlength="6" />
                </p>

            <?php else : ?>

                <p><?php esc_html_e( 'A password will be sent to your email address.', 'dokan-theme' ); ?></p>

            <?php endif; ?>

            <!-- Spam Trap -->
            <div style="left:-999em; position:absolute;"><label for="trap"><?php esc_html_e( 'Anti-spam', 'dokan-theme' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

            <?php do_action( 'woocommerce_register_form' ); ?>
            <?php do_action( 'register_form' ); ?>

            <p class="form-row">
                <?php wp_nonce_field( 'woocommerce-register', '_wpnonce' ); ?>
                <button type="submit" class="dokan-btn dokan-btn-theme" name="register" value="<?php esc_attr_e( 'Register', 'dokan-theme' ); ?>"> <?php _e( 'Register', 'dokan-theme' ); ?> </button>
            </p>

            <?php do_action( 'woocommerce_register_form_end' ); ?>

        </form>

    </div>

</div>


<?php do_action( 'woocommerce_after_customer_login_form' ); ?>

   <?php

   return ob_get_clean();
}