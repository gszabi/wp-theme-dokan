<?php

// WALKER
if ( !class_exists( 'Dokan_Custom_Category_Walker' ) ) {

    /**
     * Category walker for generating dokan category
     */
    class Dokan_Custom_Category_Walker extends Walker {

        var $tree_type = 'category';
        var $db_fields = array( 'parent' => 'parent', 'id' => 'term_id' ); //TODO: decouple this


        function start_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat( "\t", $depth );

            if ( $depth == 0 ) {
                $output .= $indent . '<ul class="children level-' . $depth . '">' . "\n";
            } else {
                $output .= "$indent<ul class='children level-$depth'>\n";
            }
        }

        function end_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat( "\t", $depth );

            if ( $depth == 0 ) {
                $output .= "$indent</ul> <!-- .sub-category -->\n";
            } else {
                $output .= "$indent</ul>\n";
            }
        }

        function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
            extract( $args );
            $indent = str_repeat( "\t\r", $depth );
            $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );
            if ( $depth == 0 ) {
                $caret      = $args['has_children'] ? ' <span class="caret-icon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>' : '';
                $class_name = $args['has_children'] ? ' class="has-children parent-cat-wrap"' : ' class="parent-cat-wrap"';
                $output     .= $indent . '<li' . $class_name . '><a href="' . get_term_link( $category ) . '"><span class="cat-icon" style="background-image: url('.$image.');"></span>' . $category->name . $caret . '</a>' . "\n";
            } else {
                $caret      = $args['has_children'] ? ' <span class="caret-icon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>' : '';
                $class_name = $args['has_children'] ? ' class="has-children"' : '';
                $output     .= $indent . '<li' . $class_name . '><a href="' . get_term_link( $category ) . '">' . $category->name . $caret . '</a>';
            }
        }

        function end_el( &$output, $category, $depth = 0, $args = array() ) {
            $indent = str_repeat( "\t", $depth );

            if ( $depth == 1 ) {
                $output .= "$indent</li><!-- .sub-block -->\n";
            } else {
                $output .= "$indent</li>\n";
            }
        }

    }

}

// WIDGET

if ( !class_exists( 'Dokan_Custom_Category_Widget' ) ) :

    /**
     * new WordPress Widget format
     * Wordpress 2.8 and above
     * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
     */
    class Dokan_Custom_Category_Widget extends WP_Widget {

        /**
         * Constructor
         *
         * @return void
         * */
        public function __construct() {
            $widget_ops = array( 'classname' => 'dokan-custom-category-menu', 'description' => __( 'Dokan product category menu', 'dokan-theme' ) );
            parent::__construct( 'dokan-custom-category-menu', 'Dokan: Custom Product Category', $widget_ops );
        }

        /**
         * Outputs the HTML for this widget.
         *
         * @param array  An array of standard parameters for widgets in this theme
         * @param array  An array of settings for this widget instance
         * @return void Echoes it's output
         * */
        function widget( $args, $instance ) {
            extract( $args, EXTR_SKIP );

            $title = apply_filters( 'widget_title', $instance['title'] );

            echo $before_widget;

            if ( !empty( $title ) )
                echo $args['before_title'] . $title . $args['after_title'];
            ?>
            <div id="front-cat-drop-stack">
                <?php
                $args = apply_filters( 'dokan_custom_category_widget', array(
                    'hide_empty' => false,
                    'orderby'    => 'name',
                ) );

                $categories = get_terms( 'product_cat', $args );

                $args = array(
                    'taxonomy'      => 'product_cat',
                    'selected_cats' => ''
                );

                $walker = new Dokan_Custom_Category_Walker();
                echo "<ul>";
                echo call_user_func_array( array( &$walker, 'walk' ), array( $categories, 0, array() ) );
                echo "</ul>";
                ?>
            </div>
            <script>
                    ( function ( $ ) {
                        function toggleCatMenu(){
                            if ( $( 'body' ).hasClass( 'isOverlayActive' ) ){
                                $( 'body' ).removeClass( 'isOverlayActive' );
                                $( '#front-cat-drop-stack>ul>li.isOpen' ).removeClass('isOpen');
                            }
                        }

                        $( document ).on( 'click', 'div.nav-cover',function (event) {
                            toggleCatMenu();
                        });

                        $( '#front-cat-drop-stack li.has-children.parent-cat-wrap' ).on( 'click', '> a span.caret-icon', function ( e ) {
                            e.preventDefault();
                            var self = $( this ),
                                liHasChildren = self.closest( 'li.has-children' );

                                if ( !liHasChildren.hasClass('isOpen') ) {
                                    liHasChildren.toggleClass('isOpen').siblings().removeClass('isOpen');
                                }else{
                                    liHasChildren.removeClass('isOpen');
                                    liHasChildren.parent().find('li.isOpen').removeClass("isOpen");

                                }

                                if ( liHasChildren.hasClass('isOpen') ) {
                                    $('body').addClass('isOverlayActive');

                                } else {
                                    $('body').removeClass('isOverlayActive');
                                }

                        /*$( '#front-cat-drop-stack li.has-children' ).on( 'click', '> a span.caret-icon', function ( e ) {
                            e.preventDefault();
                            var self = $( this ),
                                liHasChildren = self.closest( 'li.has-children' );

                                if ($('body').hasClass('isOverlayActive')) {
                                    $('body').removeClass('isOverlayActive');
                                } else {
                                    $('body').addClass('isOverlayActive');
                                }

                            if ( !liHasChildren.find( '> ul.children' ).is( ':visible' ) ) {
                                self.find( 'i.fa' ).addClass( 'fa-rotate-180' );
                                if ( liHasChildren.find( '> ul.children' ).hasClass( 'level-0' ) ) {
                                    self.closest( 'a' ).css( { 'borderBottom': 'none' } );
                                }
                            }

                            ///itt
                            liHasChildren.find( '> ul.children' ).slideToggle( 'fast', function () {
                                if ( !$( this ).is( ':visible' ) ) {
                                    self.find( 'i.fa' ).removeClass( 'fa-rotate-180' );
                                    if ( liHasChildren.find( '> ul.children' ).hasClass( 'level-0' ) ) {
                                        //self.closest( 'a' ).css( { 'borderBottom': '1px solid #eee' } );
                                    }
                                }
                            } );
                            */
                            /*
                            liHasChildren.find( '> ul.children' ).slideToggle({
                                duration: 'fast',
                                step: function() {
                                    if ($(this).css('display') == 'block') {
                                        $(this).css('display', 'flex');
                                    }
                                },
                                complete: function() {
                                    if ($(this).css('display') == 'block') {
                                        $(this).css('display', 'flex');
                                    }
                                    if ( !$(this ).is( ':visible' ) ) {
                                        self.find( 'i.fa' ).removeClass( 'fa-rotate-180' );
                                        if ( liHasChildren.find( '> ul.children' ).hasClass( 'level-0' ) ) {
                                            //self.closest( 'a' ).css( { 'borderBottom': '1px solid #eee' } );
                                        }
                                    }
                                }
                            });
                            */
                        } ); // enyem
                    } )( jQuery );
            </script>
            <?php
            echo $after_widget;
            ?>
            <div class="nav-cover"></div>
            <?php

        }

        /**
         * Deals with the settings when they are saved by the admin. Here is
         * where any validation should be dealt with.
         *
         * @param array  An array of new settings as submitted by the admin
         * @param array  An array of the previous settings
         * @return array The validated and (if necessary) amended settings
         * */
        function update( $new_instance, $old_instance ) {

            // update logic goes here
            $updated_instance = $new_instance;
            return $updated_instance;
        }

        /**
         * Displays the form for this widget on the Widgets page of the WP Admin area.
         *
         * @param array  An array of the current settings for this widget
         * @return void Echoes it's output
         * */
        function form( $instance ) {
            $instance = wp_parse_args( (array) $instance, array(
                'title' => __( 'Product Category', 'dokan-theme' )
            ) );

            $title = $instance['title'];
            ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'dokan-theme' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
            </p>
            <?php
        }

    }

    add_action( 'widgets_init', 'dokan_theme_register_custom_widget' );

    function dokan_theme_register_custom_widget() {
        register_widget( 'Dokan_Custom_Category_Widget' );
    }

endif;

// FUNCTION
if ( ! function_exists( 'dokan_custom_category_widget' ) ) :

    /**
     * Display the product category widget
     *
     * @return void
     */
    function dokan_custom_category_widget() {
            if ( class_exists( 'Dokan_Custom_Category_Widget' ) ) {
                the_widget( 'Dokan_Custom_Category_Widget', array(
                    //'title' => __( 'Product Categories', 'dokan-theme' )
                ), array(
                    'before_widget' => '<aside class="widget front-category-menu">',
                    'after_widget'  => '</aside>',
                    'before_title'  => '<h3 class="widget-title">',
                    'after_title'   => '</h3>',
                )
                );
            }
    }

    endif;