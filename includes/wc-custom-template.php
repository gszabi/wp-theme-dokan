<?php
function keszleten( $s, $q = null ){
    // instock, outofstock, onbackorder
    switch ($s) {
        case 'instock':
            $r = 'Raktáron';
            $c = 'success';
            break;
        case 'outofstock':
            $r = 'Elfogyott';
            $c = 'danger';
            break;
        case 'onbackorder':
            $r = 'Rendelhető';
            $c = 'warning';
            break;
        default:
            $r = 'Ismeretlen';
            $c = 'default';
            break;
    }
    $qi = ( $q != null ) ? ' <span class="badge">'.$q.'</span>' : '';
    $html = "<span class=\"label label-{$c}\">{$r}</span>{$qi}";
    echo $html;
}

function kereso( $pull = null) {
    $pullclass = ( isset( $pull ) ? 'pull-'.$pull : '');
    ?>
    <form role="search" method="get" class="woocommerce-product-search <?php echo $pullclass; ?>" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
        <input type="search" class="search-field" placeholder="Termék keresése..." value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />
        <input type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>" />
        <input type="hidden" name="post_type" value="product" />
    </form>
    <?php
}