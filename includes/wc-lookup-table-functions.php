<?php
/**
 * WC_Dokan_Lookup_Table_Fix class file.
 *
 * @author Sabolch Greba
 */

class WC_Dokan_Lookup_Table_Fix extends WC_Product_Data_Store_CPT {
    public function dokan_add_product_to_lookup_table( $post_id ) {
        $this->update_lookup_table($post_id, 'wc_product_meta_lookup');
    }
}

function dokan_add_product_to_lookup_table( $post_id ) {
    $lookup = new WC_Dokan_Lookup_Table_Fix();
    $lookup->dokan_add_product_to_lookup_table( $post_id );
}
add_action( 'dokan_new_product_added', 'dokan_add_product_to_lookup_table', 11 );
