<?php
/**
 * The main template file for homepage.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dokan
 * @package dokan - 2014 1.0
 */
get_header();
?>

<div id="primary" class="home-content-area col-md-12">
    <div id="content" class="site-content" role="main">
        <div class="row">
            <div class="col-md-12">
                <?php dokan_custom_category_widget(); ?>
            </div>
        </div> <!-- #home-page-section-1 -->

        <?php if ( function_exists( 'dokan_custom_product_list' ) ) : ?>
            <?php //dokan_custom_product_list(); ?>
        <?php
            endif;
            woocommerce_output_all_notices();
        ?>

        <p><?php kereso(); ?></p>
        <?php
            if(!function_exists('wc_get_products')) {
                return;
            }
            // page var, ha az oldal front-page.php (nem paged) + statikus kezdolapot kell beallitani
            $paged                   = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
            $paged                   = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
            $ordering                = WC()->query->get_catalog_ordering_args();
            $ordering['orderby']     = array_shift(explode(' ', $ordering['orderby']));
            $ordering['orderby']     = stristr($ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
            //$products_per_page       = apply_filters('loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page());
            $perpagebyloop           =   wc_get_loop_prop('per_page');
            $products_per_page_var       = apply_filters('loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page());
            if ( is_numeric($perpagebyloop) ) {
                $products_per_page = wc_get_loop_prop('per_page');
            }else{
                $products_per_page = $products_per_page_var;
            }


            $products_ids            = wc_get_products(array(
                'status'               => 'publish',
                'limit'                => $products_per_page,
                'paged'                => $paged,
                'paginate'             => true,
                'return'               => 'ids',
                'orderby'              => $ordering['orderby'],
                'order'                => $ordering['order'],
            ));

            wc_set_loop_prop('current_page', $paged);
            wc_set_loop_prop('is_paginated', wc_string_to_bool(true));
            wc_set_loop_prop('page_template', get_page_template_slug());
            wc_set_loop_prop('per_page', $products_per_page);
            wc_set_loop_prop('total', $products_ids->total);
            wc_set_loop_prop('total_pages', $products_ids->max_num_pages);

            if($products_ids) {
                //do_action('woocommerce_before_shop_loop');
                woocommerce_product_loop_start();
                foreach($products_ids->products as $featured_product) {
                    $post_object = get_post($featured_product);
                    setup_postdata($GLOBALS['post'] =& $post_object);
                    wc_get_template_part('content', 'product');
                }
                wp_reset_postdata();
                woocommerce_product_loop_end();
                do_action('woocommerce_after_shop_loop');
            } else {
                do_action('woocommerce_no_products_found');
            }
        ?>
    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->


<?php

get_footer();