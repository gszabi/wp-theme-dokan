<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="form-group">
	<?php woocommerce_catalog_ordering(); ?>
</div>
<div class="table-responsive">
<table class="table table-striped table-hover products columns-<?php echo esc_attr( wc_get_loop_prop( 'columns' ) ); ?> termekek">
<thead>
	<tr>
		<th>Termék</th>
		<th>Hely</th>
		<th>Kategória</th>
		<th>Készlet</th>
		<th>Ár</th>
		<th class="text-center">Mny</th>
		<th><input class="product-select-all" type="checkbox" value=""></th>
		<th></th>
	</tr>
</thead>
<tbody>
