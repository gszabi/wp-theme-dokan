<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$product_id 	= $product->get_id();
$vendor_id 		= $product->post->post_author;
$link 			= get_permalink( $product_id );
$author 		= get_user_by( 'id', $vendor_id );
$store_info 	= dokan_get_store_info( $author->ID );
$jaras 			= $store_info['address']['district'];
$jaras 			= ( !empty($jaras) ) ? jarasok( $jaras ) : '';
$hely 			= ( !empty($jaras) ) ? $jaras : $store_info['address']['city'];
?>
<tr id="product_id_<?php echo $product_id; ?>">
	<td><strong><a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a></strong><br><small class="text-muted"><?php echo $store_info['store_name']; ?></small></td>
	<td><?php echo $hely; ?></td>
	<td><?php echo $product->get_categories(); ?></td>
	<td><?php keszleten( $product->get_stock_status(), $product->get_stock_quantity() ); ?></td>
	<td><?php woocommerce_template_loop_price(); // wc_price( $product->get_price() ); ?></td>
	<td class="termek-mny text-center" data-product_id="<?php echo $product_id; ?>">
	<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input(
			array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			)
		);

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>
	</td>
	<td><input class="product-checkbox" type="checkbox" value="" data-product_id="<?php echo $product_id; ?>"></td>
	<td><?php woocommerce_template_loop_add_to_cart(); ?></td>



</tr>
