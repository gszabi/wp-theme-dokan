<?php
/**
 * Dokan functions and definitions
 *
 * @package Dokan
 * @since Dokan 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Dokan 1.0
 */

if ( !isset( $content_width ) ) {
    $content_width = 640;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @since Dokan 1.0
 */
class WeDevs_Dokan_Theme {

    function __construct() {

        //includes file
        $this->includes();

        // init actions and filter
        $this->init_filters();
        $this->init_actions();

        // initialize classes
        $this->init_classes();
    }

    /**
     * Initialize filters
     *
     * @return void
     */
    function init_filters() {
        add_filter( 'wp_title', array( $this, 'wp_title' ), 10, 2 );
        add_filter( 'dokan_ls_theme_tags', array( $this, 'live_search_support' ), 10, 2 );
    }

    /**
     * Init action hooks
     *
     * @return void
     */
    function init_actions() {
        add_action( 'after_setup_theme', array( $this, 'setup' ) );
        add_action( 'widgets_init', array( $this, 'widgets_init' ) );

        add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
        add_action( 'dokan_admin_menu', array( $this, 'slider_page' ) );
    }

    public function init_classes() {
        Dokan_Slider::init();
    }


    function includes() {
        $lib_dir     = dirname( __FILE__ ) . '/lib/';
        $inc_dir     = dirname( __FILE__ ) . '/includes/';
        $classes_dir = dirname( __FILE__ ) . '/classes/';

        require_once $classes_dir . 'slider.php';

        require_once $inc_dir . 'wc-functions.php';
        require_once $inc_dir . 'wc-template.php';
        require_once $inc_dir . 'wc-custom-template.php';
        // Custom Includes
        require_once $inc_dir . 'dokan-custom-reg.php';
        require_once $inc_dir . 'dokan-custom-category-list.php';
        require_once $inc_dir . 'dokan-custom-product-list.php';
        require_once $inc_dir . 'wc-lookup-table-functions.php';

        if ( is_child_theme() && file_exists( get_stylesheet_directory() . '/classes/customizer.php' ) ) {
            require_once get_stylesheet_directory() . '/classes/customizer.php';
        } else {
            require_once $classes_dir . 'customizer.php';
        }

        if ( is_admin() ) {

        } else {
            require_once $lib_dir . 'bootstrap-walker.php';
            require_once $inc_dir . 'template-tags.php';
        }
    }

    /**
     * Setup dokan
     *
     * @uses `after_setup_theme` hook
     */
    function setup() {

        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         */
        load_theme_textdomain( 'dokan-theme', get_template_directory() . '/languages' );

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support( 'automatic-feed-links' );

        /**
         * Enable support for Post Thumbnails
         */
        add_theme_support( 'post-thumbnails' );

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus( array(
            'primary'  => __( 'Primary Menu', 'dokan-theme' ),
            'top-left' => __( 'Top Left', 'dokan-theme' ),
            'footer'   => __( 'Footer Menu', 'dokan-theme' ),
        ) );

        add_theme_support( 'woocommerce' );

        // Support gallery image for single product page
        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );

        /*
         * This theme supports custom background color and image,
         * and here we also set up the default background color.
         */
        add_theme_support( 'custom-background', array(
            'default-color' => 'F7F7F7',
        ) );

        add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
    }

    /**
    * Live search module on support
    *
    * @since 2.3.1
    *
    * @return void
    **/
    public function live_search_support( $themes ) {
        $themes['dokan-theme'] = '#primary';
        return $themes;
    }

    /**
     * Register widgetized area and update sidebar with default widgets
     *
     * @since Dokan 1.0
     */
    function widgets_init() {

        $sidebars = array(
            array( 'name' => __( 'General Sidebar', 'dokan-theme' ), 'id' => 'sidebar-1' ),
            array( 'name' => __( 'Home Sidebar', 'dokan-theme' ), 'id' => 'sidebar-home' ),
            array( 'name' => __( 'Blog Sidebar', 'dokan-theme' ), 'id' => 'sidebar-blog' ),
            array( 'name' => __( 'Shop Archive', 'dokan-theme' ), 'id' => 'sidebar-shop' ),
            array( 'name' => __( 'Single Product', 'dokan-theme' ), 'id' => 'sidebar-single-product' ),
            array( 'name' => __( 'Footer Sidebar - 1', 'dokan-theme' ), 'id' => 'footer-1' ),
            array( 'name' => __( 'Footer Sidebar - 2', 'dokan-theme' ), 'id' => 'footer-2' ),
            array( 'name' => __( 'Footer Sidebar - 3', 'dokan-theme' ), 'id' => 'footer-3' ),
            array( 'name' => __( 'Footer Sidebar - 4', 'dokan-theme' ), 'id' => 'footer-4' ),
        );

        $args = apply_filters( 'dokan_widget_args', array(
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) );

        foreach ( $sidebars as $sidebar ) {

            $args['name'] = $sidebar['name'];
            $args['id'] = $sidebar['id'];

            register_sidebar( $args );
        }
    }

    /**
     * Enqueue scripts and styles
     *
     * @since Dokan 1.0
     */
    function scripts() {

        $protocol           = is_ssl() ? 'https' : 'http';
        $template_directory = get_template_directory_uri();
        $skin               = get_theme_mod( 'color_skin', 'orange.css' );

        wp_register_style( 'dokan-fontawesome', $template_directory . '/assets/css/font-awesome.min.css', false, null );

        // register styles
        wp_enqueue_style( 'bootstrap', $template_directory . '/assets/css/bootstrap.css', false, null );
        wp_enqueue_style( 'flexslider', $template_directory . '/assets/css/flexslider.css', false, null );
        wp_enqueue_style( 'dokan-fontawesome' );
        wp_enqueue_style( 'dokan-opensans', $protocol . '://fonts.googleapis.com/css?family=Open+Sans:400,700' );
        wp_enqueue_style( 'dokan-theme', $template_directory . '/style.css', false, null );
        wp_enqueue_style( 'dokan-theme-skin', $template_directory . '/assets/css/skins/' . $skin, false, null );

        /****** Scripts ******/
        if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

        if ( is_singular() && wp_attachment_is_image() ) {
            wp_enqueue_script( 'keyboard-image-navigation', $template_directory . '/assets/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
        }

        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'jquery-ui' );

        wp_enqueue_script( 'bootstrap-min', $template_directory . '/assets/js/bootstrap.min.js', false, null, true );
        wp_enqueue_script( 'flexslider', $template_directory . '/assets/js/jquery.flexslider-min.js', array( 'jquery' ) );

        wp_enqueue_script( 'dokan-theme-scripts', $template_directory . '/assets/js/script.js', false, null, true );
    }

    /**
     * Create a nicely formatted and more specific title element text for output
     * in head of document, based on current view.
     *
     * @since Dokan 1.0.4
     *
     * @param string  $title Default title text for current view.
     * @param string  $sep   Optional separator.
     * @return string The filtered title.
     */
    function wp_title( $title, $sep ) {
        global $paged, $page;

        if ( is_feed() ) {
            return $title;
        }

        // Add the site name.
        $title .= get_bloginfo( 'name' );

        // Add the site description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) ) {
            $title = "$title $sep $site_description";
        }

        // Add a page number if necessary.
        if ( $paged >= 2 || $page >= 2 ) {
            $title = "$title $sep " . sprintf( __( 'Page %s', 'dokan-theme' ), max( $paged, $page ) );
        }

        return $title;
    }

    public function slider_page() {
        add_submenu_page( 'themes.php', __( 'Slider', 'dokan-theme' ), __( 'Slider', 'dokan-theme' ), 'manage_options', 'edit.php?post_type=dokan_slider' );
    }

}

$dokan = new WeDevs_Dokan_Theme();

add_filter( 'terms_clauses', function (  $pieces, $taxonomies, $args )
{
    // Check if our custom arguments is set and set to 1, if not bail
    if (    !isset( $args['exclude_top'] )
         || 1 !== $args['exclude_top']
    )
        return $pieces;

    // Everything checks out, lets remove parents
    $pieces['where'] .= ' AND tt.parent > 0';

    return $pieces;
}, 10, 3 );

// Unit Of Measurement meta update after dokan_product_updated
function uom_save_field_input( $post_id ) {
    // IF EDIT
    if ( isset( $_POST['_woo_uom_input'], $_POST['dokan_edit_product_nonce'] ) && wp_verify_nonce( sanitize_key( $_POST['dokan_edit_product_nonce'] ), 'dokan_edit_product' ) ) :
        $woo_uom_input = sanitize_text_field( wp_unslash( $_POST['_woo_uom_input'] ) );
        update_post_meta( $post_id, '_woo_uom_input', esc_attr( $woo_uom_input ) );
    endif;
    // IF NEW
    if ( isset( $_POST['_woo_uom_input'], $_POST['dokan_add_new_product_nonce'] ) && wp_verify_nonce( sanitize_key( $_POST['dokan_add_new_product_nonce'] ), 'dokan_add_new_product' ) ) :
        $woo_uom_input = sanitize_text_field( wp_unslash( $_POST['_woo_uom_input'] ) );
        add_post_meta( $post_id, '_woo_uom_input', esc_attr( $woo_uom_input ) );
    endif;
}

add_action( 'dokan_product_updated', 'uom_save_field_input', 10 );
add_action( 'dokan_process_product_meta', 'uom_save_field_input', 10 );
add_action( 'dokan_new_product_added', 'uom_save_field_input', 10 );
add_filter( 'woocommerce_get_price_html', 'uom_render_output', 10, 2 );

function uom_render_output( $price ) {
    global $post;
    // Check if uom text exists.
    $woo_uom_output = get_post_meta( $post->ID, '_woo_uom_input', true );
    // Check if variable OR UOM text exists.
    if ( $woo_uom_output ) :
        $price = $price . ' / <span class="uom">' . esc_attr( $woo_uom_output, 'woocommerce-uom' ) . '</span>';
        return $price;
    else :
        return $price;
    endif;
}

add_filter('woocommerce_catalog_orderby', 'wc_customize_product_sorting');

function wc_customize_product_sorting($sorting_options){
    $sorting_options = array(
        'menu_order' => __( 'Default sorting', 'woocommerce' ),
        'date'       => __( 'Sort by latest', 'woocommerce' ),
        'price'      => __( 'Sort by price: low to high', 'woocommerce' ),
        'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),
    );

    return $sorting_options;
}


function ps_selectbox() {
    $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
    echo '<div class="woocommerce-perpage pull-right">';
    echo '<select onchange="if (this.value) window.location.href=this.value">';
    $orderby_options = array(
        '10' => '10',
        '15' => '15',
        '25' => '25',
        '50' => '50'
    );
    foreach( $orderby_options as $value => $label ) {
        echo "<option ".selected( $per_page, $value )." value='?perpage=$value'>$label</option>";
    }
    echo '</select>';
    echo '<span> találat megjelenítése</span>';
    echo '</div>';
}

add_action( 'pre_get_posts', 'ps_pre_get_products_query' );
function ps_pre_get_products_query( $query ) {
   $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
   if( !is_admin() && is_front_page() ){
        $query->set( 'posts_per_page', $per_page );
        if ( !is_post_type_archive( 'product' )) wc_set_loop_prop('per_page', $per_page);
    }
}

function jarasok( $param ) {
    $options = array(
        'beregszasz' => 'Beregszászi járás',
        'huszt' => 'Huszti járás',
        'ilosva' => 'Ilosvai járás',
        'munkacs' => 'Munkácsi járás',
        'nagyberezna' => 'Nagybereznai járás',
        'nagyszolos' => 'Nagyszőlősi járás',
        'okormezo' => 'Ökörmezői járás',
        'perechen' => 'Perecsenyi járás',
        'raho' => 'Rahói járás',
        'szolyva' => 'Szolyvai járás',
        'tecso' => 'Técsői járás',
        'ungvar' => 'Ungvári járás',
        'voloc' => 'Volóci járás'
    );

    if ( empty($param) ){
        return false;
    }else{
        if ( $param == 'get' ) {
            return $options;
        } else {
            return $options[$param];
        }
    }


}